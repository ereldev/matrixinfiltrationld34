class TaskBehavior extends Sup.Behavior {
    public name: string;
    public duration: number;

    private progressValue: Sup.Actor;
    private progressNeedUpdate: number = 0;
    private totalProgressDuration: number = 0;

    awake()
    {
        this.progressValue = this.actor.getChild("Value");
    }

    update() {
        if(this.duration != 0) {
            if(this.duration % 5 == 0) {
                var scale = (this.totalProgressDuration-this.duration) / this.totalProgressDuration;
                if(scale != 0) {
                    this.progressValue.setVisible(true);
                    this.progressValue.setLocalScaleX(scale);
                }
                else
                    this.progressValue.setVisible(false);
            }
            this.duration--;
            if(this.duration <= 0)
                this.actor.getParent().getBehavior(UIBehavior).removeTask(this.actor);
        }
    }
    
    public initTask(name: string, duration: number)
    {
        this.name = Lang.get(name);
        this.duration = duration + 1; // Pour eviter que l'auto remove se fasse avant le remove manuel
        this.actor.textRenderer.setText(this.name);
        this.totalProgressDuration = this.duration;
    }
}
Sup.registerBehavior(TaskBehavior);
