class TextInfoBehavior extends Sup.Behavior {
    
    private clign: number = 0;
    private sens: number = 1;
    private remove: number = 3;

    update() {
        this.clign += this.sens;
        if (this.clign >= 1 * 60 && this.sens == 1)
        {
            this.actor.setVisible(false);
            this.sens = -1;
            this.remove--;
        }
        else if (this.clign <= 0 && this.sens == -1)
        {
            this.actor.setVisible(true);
            this.sens = 1;
        }
        if (this.remove == 0)
            this.actor.destroy();
    }

    public initText(text: string)
    {
        this.actor.textRenderer.setText(Lang.get(text));
    }
}
Sup.registerBehavior(TextInfoBehavior);
