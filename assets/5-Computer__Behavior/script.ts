class ComputerBehavior extends Sup.Behavior {
    
    public x: number = 0;
    public y: number = 0;

    public isServer: boolean = false;
    public isPlayer: boolean = false;

    public IP: string;

    public firewallLevel: number = 1;
    public antivirusLevel: number = 1;
    public scanLevel: number = 1;
    public hackLevel: number = 1;
    public cleanLevel: number = 1;
    public cpuLevel: number = 1;

    public cashPerSecond: number = 0;

    public isScan: boolean = false;
    public isHacked: boolean = false;
    public isPolicyScan: boolean = false;

    private parent: ComputerBehavior;
    private wire: Sup.Actor;
    private children: ComputerBehavior[] = [];

    public from: ComputerBehavior;
    public currentTask: Sup.Actor;

    private totalProgressDuration: number = 0;
    private progressDuration: number = 0;

    private policyScanDuration: number = 1200;
    private currentPolicyScan: number = 0;
    private policyScanCallback: any;
    
    private progress: Sup.Actor;
    private progressValue: Sup.Actor;
    private progressCallback: any;
    private progressCanceled: boolean = false;

    private selector: Sup.Actor;
    private alert: Sup.Actor;

    private network: NetworkBehavior;
    private ui: UIBehavior;

    awake() {
        this.progress = this.actor.getChild("Progress");
        this.progressValue = this.actor.getChild("Value");
        this.selector = this.actor.getChild("Selector");
        this.alert = this.actor.getChild("Alert");
        
        this.network = Sup.getActor("Network").getBehavior(NetworkBehavior);
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
    }

    start() {
        if(this.isPlayer) {
            this.IP = "127.0.0.1";
            this.isScan = true;
            this.isHacked = true;
        }
        else {
            this.IP = "IP: " + Sup.Math.Random.integer(0, 255).toString() + "." + Sup.Math.Random.integer(0, 255).toString() + "." + Sup.Math.Random.integer(0, 255).toString() + "." + Sup.Math.Random.integer(0, 255).toString();
            
            var sprite = this.isServer ? "Computer/Server": "Computer/Computer";
            this.actor.spriteRenderer.setSprite(sprite);
            this.actor.spriteRenderer.setAnimation("Unknown");
        }
    }

    update() {
        if(this.isPolicyScan) {
            this.currentPolicyScan--;
            if(this.currentPolicyScan <= 0) {
                this.isPolicyScan = false;
                this.alert.setVisible(false);
                
                if(this.currentTask != null) {
                    this.ui.removeTask(this.currentTask);
                    this.currentTask = null;
                }
                
                this.policyScanCallback();
                this.progressDuration = 1; // Cause event end
                this.progressCanceled = true;
            }
        }
        
        if(this.totalProgressDuration != 0) {
            if(this.from != null && this.from.isConnected() == false) {
                this.progressDuration = 1; // Cause event end
                this.progressCanceled = true;
                this.from = null;
                
                if(this.currentTask != null) {
                    this.ui.removeTask(this.currentTask);
                    this.currentTask = null;
                }
            }
            
            if(this.progressDuration % 5 == 0) {
                var scale = (this.totalProgressDuration-this.progressDuration) / this.totalProgressDuration;
                if(scale != 0) {
                    this.progressValue.setVisible(true);
                    this.progressValue.setLocalScaleX(scale);
                }
                else
                    this.progressValue.setVisible(false);
            }

            this.progressDuration--;
            if(this.progressDuration == 0) {
                this.currentTask = null;
                this.from = null;
                
                this.totalProgressDuration = 0;
                this.progressCallback();
                this.progress.setVisible(false);
                
                if(this.progressCanceled == false)
                    Global.playSound("Sounds/Task");
                else
                    this.progressCanceled = false;
            }
        }
    }

    public startScan(scanLevel, callback: any): number {
        var duration = this.firewallLevel * 160 - scanLevel * 80;
        if(duration < 160) duration = 160;
        
        this.startProgress(duration, () => {
            this.actor.spriteRenderer.setAnimation("Scan");
            this.isScan = true;
            callback();
        });
        
        return duration;
    }

    public startHack(hackLevel, callback: any): number {
        var duration = this.antivirusLevel * 240 - hackLevel * 120;
        if(duration < 240) duration = 240;
        
        this.startProgress(duration, () => {
            callback();
        });
        
        return duration;
    }

    public validHack() {
        this.actor.spriteRenderer.setAnimation("Hack");
        this.isHacked = true;
    }

    public startClean(cleanLevel: number, callback: any): number {
        var duration = this.scanLevel * 240 - cleanLevel * 120;
        if(duration < 240) duration = 240;
        
        this.startProgress(duration, () => {
            this.isPolicyScan = false;
            this.alert.setVisible(false);
            this.currentTask = null;
            callback();
        });
        
        return duration;
    }

    public activatePolicyScan(callback) {
        this.policyScanCallback = callback;
        this.isPolicyScan = true;
        this.currentPolicyScan = this.policyScanDuration;
        this.alert.setVisible(true);
    }

    public addChildComputer(child: ComputerBehavior) {
        this.children.push(child);
    }

    public removeChildComputer(child: ComputerBehavior) {
        var index = this.children.indexOf(child);
        this.children.splice(index, 1);
    }

    public connect(parent: ComputerBehavior, wire: Sup.Actor) {
        this.parent = parent;
        this.parent.addChildComputer(this);
        this.wire = wire;
    }

    public disconnect() {
        this.isScan = false;
        this.isHacked = false;
        this.isPolicyScan = false;
        this.actor.spriteRenderer.setAnimation("Unknown");
        
        this.wire.destroy();
        this.parent.removeChildComputer(this);
        this.parent = null;
        
        this.network.disconnectComputer(this);
        
        for(var i=this.children.length-1;i>=0;i--) {
            var child = this.children[i];
            child.disconnect();
        }
    }

    public upgrade() {
        this.firewallLevel++;
        this.antivirusLevel++;
        this.scanLevel++;
        this.hackLevel++;
        this.cleanLevel++;
        this.cpuLevel++;
    }

    public init(level: number) {
        this.firewallLevel = Sup.Math.Random.integer(level-1, level+2);
        if(this.firewallLevel < 1) this.firewallLevel = 1;
        this.antivirusLevel = Sup.Math.Random.integer(level-1, level+2);
        if(this.antivirusLevel < 1) this.antivirusLevel = 1;
        
        this.scanLevel = Sup.Math.Random.integer(level-1, level+2);
        if(this.scanLevel < 1) this.scanLevel = 1;
        this.hackLevel = Sup.Math.Random.integer(level-1, level+2);
        if(this.hackLevel < 1) this.hackLevel = 1;
        this.cleanLevel = Sup.Math.Random.integer(level-1, level+2);
        if(this.cleanLevel < 1) this.cleanLevel = 1;
        this.cpuLevel = Sup.Math.Random.integer(level-1, level+2);
        if(this.cpuLevel < 1) this.cpuLevel = 1;
        
        this.cashPerSecond = 1 + level;
    }

    public enableSelector() {
        this.selector.setVisible(true);
    }

    public disableSelector() {
        this.selector.setVisible(false);
    }

    public isIdle() { return this.totalProgressDuration != 0; }
    public isConnected() { return (this.isPlayer || this.parent != null) }

    private startProgress(duration: number, callback: any) {
        this.totalProgressDuration = this.progressDuration = duration;
        this.progressCallback = callback;
        
        this.progress.setVisible(true);
        this.progressValue.setVisible(false);
    }

}

Sup.registerBehavior(ComputerBehavior);
