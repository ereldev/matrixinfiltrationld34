class BackgroundBehavior extends Sup.Behavior {
    private spawnRate: number = 0.5;
    private time: number = 0;

    awake() {
        
    }

    update() {
        this.time += 1/60;
        
        if (this.time > this.spawnRate)
        {
            Sup.appendScene("UI/Background/Word/Prefab", this.actor);
            this.time = 0;       
        }
    }
}
Sup.registerBehavior(BackgroundBehavior);
