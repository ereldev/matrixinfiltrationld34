namespace Global {

    export let VERSION: string = "Ludum Dare";
    
    // Options
    export let enableMusic: boolean = true;
    export let enableSounds: boolean = true;
    
    // Global var
    export let score: number = 0;
    export let UIRay : Sup.Math.Ray;
    
    export function reset() {
        score = 0;
    }
    
    // Music & Sounds
    let backgroundMusic = new Sup.Audio.SoundPlayer("Sounds/Background", 0.1, { loop: true });
    
    export function playBackgroundMusic() {
        if(enableMusic)
            backgroundMusic.play();
    }
    
    export function stopBackgroundMusic() {
        backgroundMusic.stop();
    }
    
    export function playSound(sound: string) {
        if(enableSounds)
            Sup.Audio.playSound(sound, 0.1);
    }
    
}