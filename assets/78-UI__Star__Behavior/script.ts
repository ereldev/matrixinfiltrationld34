class StarBehavior extends Sup.Behavior {

    private player: PlayerBehavior;

    awake() {
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
    }

    public updateStar()
    {
        var star = this.actor.getChild("Star " + this.player.researchLevel.toString());
        star.spriteRenderer.setSprite("UI/Star/Star");
    }
}
Sup.registerBehavior(StarBehavior);
