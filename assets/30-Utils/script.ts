class Utils {
    
    public static getAngleOfLineBetweenTwoPoints(A: Sup.Math.Vector3, B: Sup.Math.Vector3): number {
        var xDiff = B.x - A.x;
        var yDiff = B.y - A.y;
        
        return Math.atan2(yDiff, xDiff);
    }
    
}
