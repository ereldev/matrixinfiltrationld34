class MenuBehavior extends Sup.Behavior {

    private helpPopup: Sup.Actor;
    private creditsPopup: Sup.Actor;
    private popupOnScreen: boolean = false;

    private camera: Sup.Camera;
    
    awake() {
        Global.enableMusic = (Sup.Storage.get("enableMusic", "1") == "1");
        Global.enableSounds = (Sup.Storage.get("enableSounds", "1") == "1");
        
        Global.UIRay = new Sup.Math.Ray(this.actor.getPosition(), new Sup.Math.Vector3(0,0,-1));
        
        this.helpPopup = Sup.getActor("HelpPopup");
        this.creditsPopup = Sup.getActor("CreditsPopup");
        
        this.camera = Sup.getActor("Camera").camera;
        
        var button = Sup.getActor("PlayButton");
        button.spriteRenderer.setSprite("UI/Button/BackgroundLarge");
        button.getChild("Text").setLocalScale(0.6, 0.6, 1);
        
        button = Sup.getActor("HelpButton");
        button.spriteRenderer.setSprite("UI/Button/BackgroundLarge");
        button.getChild("Text").setLocalScale(0.6, 0.6, 1);
        
        button = Sup.getActor("CreditsButton");
        button.spriteRenderer.setSprite("UI/Button/BackgroundLarge");
        button.getChild("Text").setLocalScale(0.6, 0.6, 1);
        
        this.updateButtons();
        
        Sup.getActor("VersionText").textRenderer.setText("Version: ".concat(Global.VERSION));
    }

    start() {
        var button = Sup.getActor("PlayButton");
        button["onclick"] = (self: ButtonBehavior) => {
            if(this.popupOnScreen)
                return;
            
            Sup.loadScene("Scene");
        };
        
        button = Sup.getActor("HelpButton");
        button["onclick"] = (self: ButtonBehavior) => {
            if(this.popupOnScreen)
                return;
            
            this.helpPopup.setVisible(true);
            this.popupOnScreen = true;
        };
        
        button = Sup.getActor("CreditsButton");
        button["onclick"] = (self: ButtonBehavior) => {
            if(this.popupOnScreen)
                return;
            
            this.creditsPopup.setVisible(true);
            this.popupOnScreen = true;
        };
        
        button = Sup.getActor("MusicButton");
        button["onclick"] = (self: ButtonBehavior) => {
            if(this.popupOnScreen)
                return;
            
            Global.enableMusic = !Global.enableMusic;
            Sup.Storage.set("enableMusic", Global.enableMusic ? "1": "0");
            
            if(Global.enableMusic)
                Global.playBackgroundMusic();
            else
                Global.stopBackgroundMusic();
            
            this.updateButtons();
        };
        
        button = Sup.getActor("SoundsButton");
        button["onclick"] = (self: ButtonBehavior) => {
            if(this.popupOnScreen)
                return;
            
            Global.enableSounds = !Global.enableSounds;
            Sup.Storage.set("enableSounds", Global.enableSounds ? "1": "0");
            
            this.updateButtons();
        };
        
        button = Sup.getActor("CloseHelpButton");
        button["onclick"] = (self: ButtonBehavior) => {
            this.helpPopup.setVisible(false);
            this.popupOnScreen = false;
        };
        
        button = Sup.getActor("CloseCreditsButton");
        button["onclick"] = (self: ButtonBehavior) => {
            this.creditsPopup.setVisible(false);
            this.popupOnScreen = false;
        };
        
        Global.playBackgroundMusic();
    }
    
    update() {
        Global.UIRay.setFromCamera(this.camera, Sup.Input.getMousePosition());
    }

    private updateButtons() {
        var button = Sup.getActor("MusicButton");
        if(Global.enableMusic)
            button.spriteRenderer.setSprite("Menu/MusicOn");
        else
            button.spriteRenderer.setSprite("Menu/MusicOff");
        
        button = Sup.getActor("SoundsButton");
        if(Global.enableSounds)
            button.spriteRenderer.setSprite("Menu/SoundsOn");
        else
            button.spriteRenderer.setSprite("Menu/SoundsOff");
    }
    
}

Sup.registerBehavior(MenuBehavior);
