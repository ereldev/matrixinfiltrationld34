class UIBehavior extends Sup.Behavior {

    private camera: Sup.Camera;
    public popupOnScreen: boolean = false;
    public popup: Sup.Actor;
    public taskList: Sup.Actor[] = [];
    private player: PlayerBehavior;

    awake() {
        this.camera = Sup.getActor("UICamera").camera;
        Global.UIRay = new Sup.Math.Ray(this.actor.getPosition(), new Sup.Math.Vector3(0,0,-1));
        Global.UIRay.setFromCamera(this.camera, Sup.Input.getMousePosition());
        this.actor.getChild("CashText").textRenderer.setText(Lang.get("Cash") + " : " + "0");
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
        this.refreshTracker();
    }

    update() {
        Global.UIRay.setFromCamera(this.camera, Sup.Input.getMousePosition());
    }

    public setScore(score: number)
    {
        this.actor.getChild("ScoreText").textRenderer.setText(Lang.get("Score").concat(score.toString()));
    }

    public setCash(cash: number)
    {
        this.actor.getChild("CashText").textRenderer.setText(Lang.get("Cash").concat(cash.toString()));
    }

    public refreshPopup()
    {
        if (this.popupOnScreen)
        {
            if (this.popup.getBehavior(PopupComputerBehavior) != null)
                this.popup.getBehavior(PopupComputerBehavior).refreshUI();
            if (this.popup.getBehavior(PopupPlayerBehavior) != null)
                this.popup.getBehavior(PopupPlayerBehavior).refreshUI();
        }
    }

    public refreshStar()
    {
        this.actor.getChild("Stars").getBehavior(StarBehavior).updateStar();
    }

    public refreshTracker()
    {
        if (this.player.getTrackerUseLeft() > 0)
        {
            this.actor.getChild("TrackerText").textRenderer.setText(this.player.getTrackerUseLeft().toString() + " x");
            this.actor.getChild("TrackerText").setVisible(true);
            this.actor.getChild("TrackerIcon").setVisible(true);
        }
        else
        {
            this.actor.getChild("TrackerText").setVisible(false);
            this.actor.getChild("TrackerIcon").setVisible(false);
        }
    }

    public addTask(name: string , duration: number): Sup.Actor
    {
        var newTask = Sup.appendScene("UI/Task/Prefab", this.actor)[0];
        var newTaskBehavior = newTask.getBehavior(TaskBehavior);
        newTaskBehavior.initTask(name, duration);
        this.taskList.push(newTask);
        this.moveTask();
        return (newTask);
    }

    private moveTask()
    {
        for (var i of this.taskList)
        {
            i.setLocalY(this.taskList.indexOf(i) / 2 - 5);
        }
    }

    public removeTask(task: Sup.Actor)
    {
        this.refreshPopup();
        var index = this.taskList.indexOf(task);
        this.taskList.splice(index, 1);
        this.moveTask();
        task.destroy();
    }

    public onComputerClick(computer: ComputerBehavior) {
        if (!this.popupOnScreen)
        {
            this.popup = Sup.appendScene("UI/Popup/Computer", this.actor)[0];
            this.popup.setLocalPosition(0, 0, 0);
            this.popup.getBehavior(PopupComputerBehavior).InitPopup(computer);
            this.popupOnScreen = true;
        }
    }
    
    public onPlayerClick(player: ComputerBehavior) {
        if (!this.popupOnScreen)
        {
            this.popup = Sup.appendScene("UI/Popup/Player", this.actor)[0];
            this.popup.setLocalPosition(0, 0, 0);
            this.popup.getBehavior(PopupPlayerBehavior).InitPopup(player);
            this.popupOnScreen = true;
        }
    }
    
    public onEventadd(eventName: string)
    {
        if (eventName == "Police")
        {
            if (this.player.haveTracker())
            {
                var textActor = Sup.appendScene("UI/TextInfo/Prefab", this.actor)[0];
                var textbehavior = textActor.getBehavior(TextInfoBehavior);
                textbehavior.initText("Alert Message");
                this.player.useTracker();
                this.refreshTracker();
                Global.playSound("Sounds/Alert");
            }
            this.refreshPopup();
        }
    }
}

Sup.registerBehavior(UIBehavior);
