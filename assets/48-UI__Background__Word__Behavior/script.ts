class WordBehavior extends Sup.Behavior {
    
    private speed: number = 0.04;
    private sens: number = 0;

    awake() {
        this.speed += Sup.Math.Random.float(-0.01, 0.01);
        this.sens = Sup.Math.Random.integer(0, 1);
        this.actor.spriteRenderer.setAnimationFrameTime(Sup.Math.Random.integer(0, 21)); 
        this.actor.spriteRenderer.pauseAnimation();
        var random = Sup.Math.Random.float(-4.8, 4.8);
        random = Math.floor(random);
        
        if (this.sens == 1)
            this.actor.setLocalPosition(-14, random + 0.5, 0);
        else
            this.actor.setLocalPosition(14, random, 0);
    }

    update() {
        if (this.sens == 1)
        {
            this.actor.moveX(this.speed);
            if (this.actor.getPosition().x > 14)
                this.actor.destroy();
        }
        else
        {
            this.actor.moveX(-this.speed);
            if (this.actor.getPosition().x < -14)
                this.actor.destroy();
        }
    }
}
Sup.registerBehavior(WordBehavior);
