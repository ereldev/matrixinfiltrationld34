class GameOverBehavior extends Sup.Behavior {

    private camera: Sup.Camera;
    
    awake() {
        this.camera = Sup.getActor("Camera").camera;
        
        var score = Sup.getActor("ScoreText").textRenderer;
        score.setText("Score: " + Global.score);
        
        var button = Sup.getActor("BackToMenu");
        button.spriteRenderer.setSprite("UI/Button/BackgroundLarge");
        button.getChild("Text").setLocalScale(0.6, 0.6, 1);
    }
    
    start() {
        var button = Sup.getActor("BackToMenu");
        button["onclick"] = (self: ButtonBehavior) => {
            Sup.loadScene("Menu/Scene");
        };
        
        Global.playBackgroundMusic();
    }
    
    update() {
        Global.UIRay.setFromCamera(this.camera, Sup.Input.getMousePosition());
    }

}

Sup.registerBehavior(GameOverBehavior);
