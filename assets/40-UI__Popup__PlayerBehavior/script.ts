class PopupPlayerBehavior extends Sup.Behavior {
    
    private ui : UIBehavior;
    private network : NetworkBehavior;
    private computer : ComputerBehavior;
    private player : PlayerBehavior;

    awake() {
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
        this.network = Sup.getActor("Network").getBehavior(NetworkBehavior);
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
        this.actor.getChild("TrackerButton").spriteRenderer.setSprite("UI/Button/BackgroundLarge");
    }
    
    start() {
        var buttonClose = this.actor.getChild("CloseButton");
        buttonClose["onclick"] = (self: ButtonBehavior) => {
            this.ui.popupOnScreen = false;
            this.removePopup();
        }
        this.initButton("Cpu");
        this.initButton("Scan");
        this.initButton("Hack");
        this.initButton("Clean");
        this.initTrackerButton();
        this.actor.getChild("CashPerSecond").textRenderer.setText(Lang.get("Cash") + " / s : " + this.player.cashPerSecond);
        this.RefreshPlayerStat();
        this.actor.setVisible(true);
    }

    update()
    {
        if (this.player.canUpgrade(this.computer.cpuLevel) || this.player.canUpgrade(this.computer.hackLevel) || this.player.canUpgrade(this.computer.scanLevel))
            this.refreshUI();
    }

    private initTrackerButton()
    {
        var button = this.actor.getChild("TrackerButton");
        button.getChild("Text").setLocalScale(0.5, 0.5, 0.5);
        var upgrade = this.player.trackerLevel;
        if (this.player.canUpgrade(upgrade))
            button.setVisible(true);
        else
            button.setVisible(false);
        this.actor.getChild("Tracker Cost").textRenderer.setText(this.player.getUpgradeCost(upgrade));
        button["onclick"] = (self: ButtonBehavior) => {
            this.player.decreaseCash(this.player.getUpgradeCost(upgrade));
            this.player.buyTracker();
            this.ui.setCash(this.player.cash);
            this.refreshUI();
            this.ui.refreshTracker();
        }
    }

    private initButton(button: string)
    {
        var upgrade: number = 0;
        if (button == "Cpu")
            upgrade = this.computer.cpuLevel;
        else if (button == "Scan")
            upgrade = this.computer.scanLevel;
        else if (button == "Hack")
            upgrade = this.computer.hackLevel;
        else if (button == "Clean")
            upgrade = this.computer.cleanLevel;
        var buttonPlus = this.actor.getChild("Plus" + button);
        if (this.player.canUpgrade(upgrade))
            buttonPlus.setVisible(true);
        else
            buttonPlus.setVisible(false);
        this.actor.getChild(button + " Level Cost").textRenderer.setText(this.player.getUpgradeCost(upgrade));
        buttonPlus.spriteRenderer.setSprite("UI/Button/ButtonPlus");
        buttonPlus.setLocalScale(0.75, 0.75, 1);
        buttonPlus["onclick"] = (self: ButtonBehavior) => {
            this.player.decreaseCash(this.player.getUpgradeCost(upgrade));
            if (button == "Cpu")
                this.computer.cpuLevel++;
            else if (button == "Scan")
                this.computer.scanLevel++;
            else if (button == "Hack")
                this.computer.hackLevel++;
            else if (button == "Clean")
                this.computer.cleanLevel++;
            this.ui.setCash(this.player.cash);
            this.refreshUI();
        }
    }

    public refreshUI()
    {
        this.start();
    }

    private removePopup()
    {
        this.ui.popupOnScreen = false;
        this.actor.destroy();
    }

    private RefreshPlayerStat()
    {
        this.actor.getChild("Hack Level").textRenderer.setText(Lang.get("HackLevel") + " : " + this.computer.hackLevel);
        this.actor.getChild("Scan Level").textRenderer.setText(Lang.get("ScanLevel") + " : " + this.computer.scanLevel);
        this.actor.getChild("Cpu Level").textRenderer.setText(Lang.get("CpuLevel") + " : " + this.computer.cpuLevel);
        this.actor.getChild("Clean Level").textRenderer.setText(Lang.get("CleanLevel") + " : " + this.computer.cleanLevel);
    }

    
    InitPopup(computer : ComputerBehavior)
    {
        this.computer = computer;
    }
}
Sup.registerBehavior(PopupPlayerBehavior);
