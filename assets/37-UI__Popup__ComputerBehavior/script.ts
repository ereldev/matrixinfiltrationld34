class PopupComputerBehavior extends Sup.Behavior {
    
    private ui : UIBehavior;
    private network : NetworkBehavior;
    private computer : ComputerBehavior;
    private player : PlayerBehavior;
    private alreadyRefresh: boolean = false;

    awake() {
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
        this.network = Sup.getActor("Network").getBehavior(NetworkBehavior);
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
    }
    
    start() {
        var buttonClose = this.actor.getChild("CloseButton");
        buttonClose["onclick"] = (self: ButtonBehavior) => {
            this.removePopup();
        }
        if (this.computer.isServer)
            this.actor.getChild("ServerCash").setVisible(true);
        this.alreadyRefresh = true;
        if (this.player.canAddTask())
        {
            this.buttonInit("Scan");
            this.buttonInit("Hack");
            this.buttonInit("Clean");
        }
        else
            this.alreadyRefresh = false;
        this.actor.setVisible(true);
    }

    public refreshUI()
    {
        this.start();
    }

    private removePopup()
    {
        this.ui.popupOnScreen = false;
        this.actor.destroy();
    }

    private buttonInit(buttonName: string)
    {
        var button = this.actor.getChild(buttonName + "Button");
        
        button.setLocalPosition(0, -3, 0);
        button.setVisible(false);
        if (this.network.isAccessible(this.computer).length > 0)
        {
            if (!this.computer.isScan && buttonName == "Scan")
            {
                button.setLocalPosition(0, -1.5, 0);
                button.setVisible(true);
                button["onclick"] = (self: ButtonBehavior) => {
                    this.network.scanComputer(this.computer);
                    this.removePopup();
                }
            }
            else if (!this.computer.isHacked && buttonName == "Hack" && this.computer.isScan)
            {
                button.setLocalPosition(0, -1.5, 0);
                button.setVisible(true);
                button["onclick"] = (self: ButtonBehavior) => {
                    this.network.hackComputer(this.computer);
                    this.removePopup();
                }
            }
            else if (this.computer.isPolicyScan && buttonName == "Clean" && this.computer.isHacked)
            {
                button.setLocalPosition(0, -1.5, 0);
                button.setVisible(true);
                button["onclick"] = (self: ButtonBehavior) => {
                    this.network.cleanComputer(this.computer);
                    this.removePopup();
                }
            }
        }
    }
    
    InitPopup(computer : ComputerBehavior)
    {
        this.computer = computer;
        if (this.computer.isScan)
        {
            this.actor.getChild("IP Text").textRenderer.setText(this.computer.IP);
            this.actor.getChild("Firewall").textRenderer.setText(Lang.get("FirewallSecurity") + " : " + this.computer.firewallLevel);
            this.actor.getChild("Antivirus").textRenderer.setText(Lang.get("AntivirusSecurity") + " : " + this.computer.antivirusLevel);
            this.actor.getChild("ServerCash").textRenderer.setText(Lang.get("Cash") + " / s : " + this.computer.cashPerSecond);
        }
    }
}
Sup.registerBehavior(PopupComputerBehavior);
