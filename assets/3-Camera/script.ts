class CameraBehavior extends Sup.Behavior {
    
    private moveSensitivity: number = 2;
    private scroolSensitivity: number = 1;
    private zoomMin: number = 10;
    private dezoomMax: number = 40;

    private network: NetworkBehavior;
    private ui: UIBehavior;

    awake() {
        this.network = Sup.getActor("Network").getBehavior(NetworkBehavior);
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
    }

    update() {
        if(this.ui.popupOnScreen)
            return;
        
        this.moveCamera();
    }
    
    private moveCamera() {
        if (Sup.Input.isMouseButtonDown(0)) {
            var delta = Sup.Input.getMouseDelta();
            
            if(delta.x >= 0.001 || delta.x <= -0.001 || delta.y >= 0.001 || delta.y <= -0.001) {
                this.network.clicked = false;
                
                var pos = this.actor.getPosition();
                var mult = this.actor.camera.getOrthographicScale() / this.moveSensitivity;
                pos.x -= (delta.x * mult);
                pos.y  -= (delta.y * mult);
                this.actor.setPosition(pos);
            }
        }
        if (Sup.Input.isMouseButtonDown(5)) {
            var scale = this.actor.camera.getOrthographicScale();
            if (scale >= this.zoomMin)
                scale -= this.scroolSensitivity;
            this.actor.camera.setOrthographicScale(scale);
        }
        if (Sup.Input.isMouseButtonDown(6)) {
            var scale = this.actor.camera.getOrthographicScale();
            if (scale <= this.dezoomMax)
                scale += this.scroolSensitivity;
            this.actor.camera.setOrthographicScale(scale);
        }
    }
}

Sup.registerBehavior(CameraBehavior);
