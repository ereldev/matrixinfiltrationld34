class EventsBehavior extends Sup.Behavior {
    
    private eventCD: number = 3600;
    private nextEvent: number = this.eventCD;
    
    private network: NetworkBehavior;
    private player: PlayerBehavior;
    private ui: UIBehavior;
    
    awake() {
        this.network = this.actor.getBehavior(NetworkBehavior);
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
    }

    update() {
        this.nextEvent--;
        if(this.nextEvent <= 0) {
            this.addEvent();
            this.nextEvent = this.eventCD;
        }
    }

    private addEvent() {
        if(this.network.hackedComputers.length == 0)
            return;
        
        var computer: ComputerBehavior = null;
        while(computer == null) {
            var index = Sup.Math.Random.integer(0, this.network.hackedComputers.length-1);
            var c = this.network.hackedComputers[index];
            if(c.x != 0 || c.y != 0)
                computer = c;
        }
        
        if(this.eventCD > 1200)
            this.eventCD -= 100;
        
        computer.activatePolicyScan(() => {
            computer.disconnect();
            computer.upgrade();
            this.player.increaseResearchLevel();
            this.ui.refreshPopup();
        });
        
        this.ui.onEventadd("Police");
    }
    
}

Sup.registerBehavior(EventsBehavior);
