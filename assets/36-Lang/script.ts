namespace Lang {
    
    var lang = {
        "CloseButton": "Close",
        "HackButton": "Hack",
        "ScanButton": "Scan",
        "CleanButton": "Clean",
        "TrackerButton": "Buy tracker",
        "HackTask": "Hack",
        "ScanTask": "Scan",
        "CleanTask": "Clean",
        "FirewallSecurity" : "Firewall",
        "AntivirusSecurity" : "Antivirus",
        "HackLevel" : "Hack  level",
        "ScanLevel" : "Scan  level",
        "CleanLevel" : "Clean level",
        "CpuLevel" : "Cpu   level",
        "PlusHack": "",
        "PlusScan": "",
        "PlusClean": "",
        "PlusCpu": "",
        "Score" : "Score: ",
        "Cash" : "Cash: ",
        "Alert Message" : "!! Scan in progress !!",
        
        "PlayButton": "Play",
        "HelpButton": "Help",
        "CreditsButton": "Credits",
        "MusicButton": "",
        "SoundsButton": "",
        "CloseHelpButton": "Close",
        "CloseCreditsButton": "Close",
        
        "BackToMenu": "Back To Menu"
    }
    
    export function get(key: string): string {
        return lang[key];
    }
    
}