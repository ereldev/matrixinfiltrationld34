class ButtonBehavior extends Sup.Behavior {
    
    public isHover : boolean = false;
    
    private color: number = 0.4;

    awake() {
        this.actor["onclick"] = null;
        this.actor["hover"] = null;
        this.actor["unhover"] = null;
        
        this.actor.getChild("Text").textRenderer.setText(Lang.get(this.actor.getName()));
    }

    update()
    {
        if (this.actor.getVisible())
        {
            if (Global.UIRay.intersectActor(this.actor, false).length > 0)
            {
                // Si hover == false, alors on re-déclare isHover a true
                if (!this.isHover) 
                {
                    this.isHover = true;
                    this.actor.spriteRenderer.setColor(this.actor.spriteRenderer.getColor().r + this.color, this.actor.spriteRenderer.getColor().g + this.color, this.actor.spriteRenderer.getColor().b + this.color);
                    this.call("hover");
                }
                // Lorsque l'on click notre bouton : 
                if (Sup.Input.wasMouseButtonJustReleased(0)) {
                    Global.playSound("Sounds/Button");
                    this.call("onclick");
                }
            }
            else
            { 
                // Si hover == true alors on re-déclare isHover a false
                if (this.isHover)
                {
                    this.isHover = false;
                    this.actor.spriteRenderer.setColor(this.actor.spriteRenderer.getColor().r - this.color, this.actor.spriteRenderer.getColor().g - this.color, this.actor.spriteRenderer.getColor().b - this.color);
                    this.call("unhover");
                }
            }
        }
    }

    private call(action: string) : void {  
        if(this.actor[action] && this.actor[action] instanceof Function) // On vérifie que l'action existe bien.
            this.actor[action](this); // => On passe le behavior en argument du callback.
    }
}
Sup.registerBehavior(ButtonBehavior);
