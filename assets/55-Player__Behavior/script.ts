class PlayerBehavior extends Sup.Behavior {
    
    public cash: number = 0;
    public cashPerSecond: number = 1;

    public taskCounter: number = 0;
    public researchLevel: number = 0;

    private trackerUse: number = 0;
    public trackerLevel: number = 1;

    private cashCD: number = 60;
    private nextCash: number = this.cashCD;

    private computer: ComputerBehavior;
    private ui: UIBehavior;

    awake() {
        this.computer = this.actor.getBehavior(ComputerBehavior);
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
    }

    update() {
        this.nextCash--;
        if(this.nextCash == 0) {
            this.cash += this.cashPerSecond;
            Global.score += (this.cashPerSecond-1);
            
            this.ui.setCash(this.cash);
            this.ui.setScore(Global.score);
            
            this.nextCash = this.cashCD;
        }
    }

    public decreaseCash(amount: number) {
        if(this.cash >= amount)
            this.cash -= amount;
    }

    public getUpgradeCost(level: number): number {
        return level * 100 + (level-1) * 50;
    }

    public buyTracker()
    {
        this.trackerLevel++;
        this.trackerUse += 5;
    }

    public haveTracker(): boolean {
        return (this.trackerUse > 0);
    }

    public useTracker() {
        this.trackerUse--;
    }

    public getTrackerUseLeft() : number
    {
        return (this.trackerUse);
    }

    public canUpgrade(level: number): boolean {
        return (this.cash >= this.getUpgradeCost(level));
    }
    public increaseResearchLevel() {
        Global.playSound("Sounds/Research");
        
        this.researchLevel++;
        
        if(this.researchLevel >= 5)
            Sup.loadScene("GameOver/Scene");
        else
            this.ui.refreshStar();
    }

    public canAddTask(): boolean {
        return (this.taskCounter < this.computer.cpuLevel);
    }
    
}

Sup.registerBehavior(PlayerBehavior);
