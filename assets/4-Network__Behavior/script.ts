let networkRay: Sup.Math.Ray;

class NetworkBehavior extends Sup.Behavior {
    
    public loading: boolean = true;
    public clicked: boolean = false;

    private grid: ComputerBehavior[][];
    private computers: Sup.Actor[];
    public hackedComputers: ComputerBehavior[] = [];
    public width: number = 21;
    public height: number = 21;

    private computerSpacement: number = 5;
    private computerPositionVariation: number = 1;

    private camera: Sup.Camera;
    private playerComputer: ComputerBehavior;
    private player: PlayerBehavior;
    private ui: UIBehavior;

    private selectMode: boolean = false;
    private accessibleComputers: Sup.Actor[];
    private target: ComputerBehavior;

    awake() {
        this.camera = Sup.getActor("Camera").camera;
        this.playerComputer = Sup.getActor("Player").getBehavior(ComputerBehavior);
        this.player = Sup.getActor("Player").getBehavior(PlayerBehavior);
        this.ui = Sup.getActor("UI").getBehavior(UIBehavior);
        
        networkRay = new Sup.Math.Ray();
        
        Global.reset();
    }

    start() {
        Global.playBackgroundMusic();
    }

    update() {
        if(Sup.Input.wasKeyJustReleased("ESCAPE")) {
            Sup.loadScene("Menu/Scene");
            return;
        }
        
        if(this.loading) {
            this.loadNetwork();
            return;
        }
        
        networkRay.setFromCamera(this.camera, Sup.Input.getMousePosition());
        
        if(Sup.Input.wasMouseButtonJustPressed(0))
            this.clicked = true;
        
        if(Sup.Input.wasMouseButtonJustReleased(0) && this.clicked) {
            if(this.selectMode) {
                this.selectMode = false;
                
                for(var c of this.accessibleComputers)
                    c.getBehavior(ComputerBehavior).disableSelector();
                
                var hits = networkRay.intersectActors(this.accessibleComputers);
                if(hits.length == 0)
                    return;
                
                var computer = hits[0].actor.getBehavior(ComputerBehavior);
                this.createLink(computer, this.target);
                Global.playSound("Sounds/Computer");
            }
            else {
                var hits = networkRay.intersectActors(this.computers);
                if(hits.length == 0)
                    return;

                var computer = hits[0].actor.getBehavior(ComputerBehavior);
                if(computer.isIdle() == false) {
                    if(this.ui.popupOnScreen == false)
                        Global.playSound("Sounds/Computer");
                    
                    if(computer.isPlayer)
                        this.ui.onPlayerClick(computer);
                    else
                        this.ui.onComputerClick(computer);
                }
            }
        }
    }

    public scanComputer(computer: ComputerBehavior) {
        var computers = this.isAccessible(computer);
        if(computers.length == 0)
            return;
        
        var duration = computer.startScan(this.playerComputer.scanLevel, () => { this.player.taskCounter--; });
        this.ui.addTask("ScanTask", duration);
        this.player.taskCounter++;
    }

    public hackComputer(computer: ComputerBehavior) {
        var computers = this.isAccessible(computer);
        if(computers.length == 0)
            return;
        
        this.target = computer;
        this.accessibleComputers = [];
        
        for(var c of computers) {
            c.enableSelector();
            this.accessibleComputers.push(c.actor);
        }
        
        this.selectMode = true;
    }

    public createLink(from: ComputerBehavior, to: ComputerBehavior) {
        if(from.isConnected() == false)
            return;
        
        var duration = to.startHack(this.playerComputer.hackLevel, () => {
            this.player.taskCounter--;
            
            if(from.isConnected() == false)
                return;
            
            if(to.isServer)
                this.player.cashPerSecond += to.cashPerSecond;
            
            var wire = this.addWire(from.actor.getPosition(), to.actor.getPosition());
            this.revealComputers(to);
            
            to.validHack();
            to.connect(from, wire);
            
            this.hackedComputers.push(to);
        });
        var task = this.ui.addTask("HackTask", duration);
        
        to.from = from;
        to.currentTask = task;
        
        this.player.taskCounter++;
    }

    public cleanComputer(computer: ComputerBehavior) {
        var duration = computer.startClean(this.playerComputer.cleanLevel, () => {
            this.ui.refreshPopup();
            this.player.taskCounter--;
        });
        var task = this.ui.addTask("CleanTask", duration);
        computer.currentTask = task;
        
        this.player.taskCounter++;
    }

    public disconnectComputer(computer: ComputerBehavior) {
        if(computer.isServer)
            this.player.cashPerSecond -= computer.cashPerSecond;
        
        var index = this.hackedComputers.indexOf(computer);
        this.hackedComputers.splice(index, 1);
    }

    public isAccessible(computer: ComputerBehavior): ComputerBehavior[] {
        var computers: ComputerBehavior[] = [];
        
        var c = this.getComputer(computer.x+1, computer.y);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x+1, computer.y+1);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x, computer.y+1);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x-1, computer.y+1);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x-1, computer.y);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x-1, computer.y-1);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x, computer.y-1);
        if(c != null && c.isHacked) computers.push(c);
        c = this.getComputer(computer.x+1, computer.y-1);
        if(c != null && c.isHacked) computers.push(c);
        
        return computers;
    }

    public getComputer(x, y) {
        if(this.grid[x] != null && this.grid[x][y] != null)
            return this.grid[x][y];
        else
            return null;
    }

    private addWire(start: Sup.Math.Vector3, end: Sup.Math.Vector3): Sup.Actor {
        var distance = start.distanceTo(end);

        var wireActor = Sup.appendScene("Wire/Prefab")[0];
        wireActor.setParent(this.actor);
        wireActor.setPosition(start);
        wireActor.setLocalScaleX(distance * 100);
        var orientation = new Sup.Math.Quaternion();
        wireActor.setEulerZ(Utils.getAngleOfLineBetweenTwoPoints(start, end));
        
        return wireActor;
    }

    private revealComputers(computer: ComputerBehavior) {
        var computers: ComputerBehavior[] = [];
        
        var c = this.getComputer(computer.x+1, computer.y);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x+1, computer.y+1);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x, computer.y+1);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x-1, computer.y+1);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x-1, computer.y);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x-1, computer.y-1);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x, computer.y-1);
        if(c != null) computers.push(c);
        c = this.getComputer(computer.x+1, computer.y-1);
        if(c != null) computers.push(c);
        
        for(var c of computers) {
            c.actor.setVisible(true);
            this.computers.push(c.actor);
        }
    }

    private loadNetwork() {
        this.grid = [];
        this.computers = [];
        
        // Init computers
        for(var x=-Math.floor(this.width/2); x<Math.floor(this.width/2)+1 ; x++) {
            this.grid[x] = [];
            
            for(var y=-Math.floor(this.height/2) ; y<Math.floor(this.height/2)+1 ; y++) {
                if(x == 0 && y == 0) {
                    this.grid[x][y] = this.playerComputer;
                    this.computers.push(this.playerComputer.actor);
                    continue;
                }
                
                var posX = Sup.Math.Random.float(this.computerSpacement * x - this.computerPositionVariation, this.computerSpacement * x + this.computerPositionVariation);
                var posY = Sup.Math.Random.float(this.computerSpacement * y - this.computerPositionVariation, this.computerSpacement * y + this.computerPositionVariation);
                
                var computerActor = Sup.appendScene("Computer/Prefab")[0];
                computerActor.setVisible(false);
                computerActor.setParent(this.actor);
                computerActor.setPosition(posX, posY, 0);
                
                var computer = computerActor.getBehavior(ComputerBehavior);
                computer.x = x;
                computer.y = y;;
                
                var level = this.getGridDistance(0, 0, x, y);
                if(level < 0) level = -level;
                level = Math.floor(level);
                computer.init(level);
                
                this.grid[x][y] = computer;
            }
        }
        
        // Init Servers
        var cluster = 3;
        
        for(var x=-Math.floor(this.width/2); x<Math.floor(this.width/2) ; x+=cluster) {
            for(var y=-Math.floor(this.height/2) ; y<Math.floor(this.height/2) ; y+=cluster) {
                // No servers on center cluster
                if(x < 0 && x+cluster > 0 && y < 0 && y+cluster > 0)
                    continue;
                
                var serverX = Sup.Math.Random.integer(x, x+cluster-1);
                var serverY = Sup.Math.Random.integer(y, y+cluster-1);
                
                if(this.grid[serverX] != null && this.grid[serverX][serverY] != null)
                    this.grid[serverX][serverY].isServer = true;
            }
        }
        
        // Reveal first computers
        this.revealComputers(this.playerComputer);
        
        this.loading = false;
    }

    private getGridDistance(x1, y1, x2, y2): number {
        var v1 = new Sup.Math.Vector3(x1, y1, 0);
        var v2 = new Sup.Math.Vector3(x2, y2, 0);
        return v1.distanceTo(v2);
    }

}

Sup.registerBehavior(NetworkBehavior);
